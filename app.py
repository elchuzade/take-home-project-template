from flask import Flask, request, jsonify
import json

app = Flask(__name__)

loggedUsers = []

@app.route('/register', methods=['POST'])
def register():
    # Input validation
    error = {}
    if (not request.json):
        error["input"] = "No input provided"
        return jsonify(error)
    if (not request.json["username"]):
        error["username"] = "Username is required"
    if (not request.json["password"]):
        error["password"] = "Password is required"
    if (error):
        return jsonify(error)

    # Open database
    with open("data.json", "r") as jsonFile:
        data = json.load(jsonFile)

    # Get user from request
    newUser = {
        "username": request.json["username"],
        "password": request.json["password"]
    }

    # Check if username exists
    for user in data["users"]:
        if (user["username"] == request.json["username"]):
            error["username"] = "Username already exists"
            return jsonify(error)

    # Add new id
    newUser["id"] = str(int(data["lastUserId"]) + 1)
    # Refresh the user counter id
    data["lastUserId"] = newUser["id"]
    # Add new user to the db
    data["users"].append(newUser)
    
    
    # Save to existing database
    with open("data.json", "w") as jsonFile:
        json.dump(data, jsonFile)

    return jsonify(newUser)


@app.route('/login', methods=['POST'])
def login():
    # Input validation
    error = {}
    if (not request.json):
        error["input"] = "No input provided"
        return jsonify(error)
    if (not request.json["username"]):
        error["username"] = "Username is required"
    if (not request.json["password"]):
        error["password"] = "Password is required"
    if (error):
        return jsonify(error)

    # Open database
    with open("data.json", "r") as jsonFile:
        data = json.load(jsonFile)

    # Get user from request
    loggedUser = {
        "username": request.json["username"],
        "password": request.json["password"]
    }

    foundUser = False
    # Check if username exists
    for user in data["users"]:
        if (user["username"] == loggedUser["username"]):
            foundUser = user

    # Check if user not found
    if (foundUser == False):
        error["user"] = "User not found"
        return jsonify(error)
    
    # Check if password is correct
    if (user["password"] != loggedUser["password"]):
        error["password"] = "Password is incorrect"
        return jsonify(error)

    loggedUsers.append(user)
    print(loggedUsers)
    return jsonify(user)


@app.route('/logout', methods=['POST'])
def logout():
    # Input validation
    error = {}
    if (not request.json):
        error["input"] = "No input provided"
        return jsonify(error)
    if (not request.json["username"]):
        error["username"] = "Username is required"
    if (not request.json["password"]):
        error["password"] = "Password is required"
    if (error):
        return jsonify(error)

    # Get user from request
    loggedUser = {
        "username": request.json["username"],
        "password": request.json["password"]
    }

    foundUser = False
    # Check if username exists
    for user in loggedUsers:
        if (user["username"] == loggedUser["username"]):
            foundUser = user

    # Check if user not found
    if (foundUser == False):
        error["user"] = "User not logged"
        return jsonify(error)
    
    # Check if password is correct
    if (user["password"] != loggedUser["password"]):
        error["password"] = "Password is incorrect"
        return jsonify(error)

    loggedUsers.remove(foundUser)
    print(loggedUsers)
    return jsonify({"message":"Thanks for using our services"})


@app.route('/properties', methods=['GET'])
def properties():
    # Open database
    with open("data.json", "r") as jsonFile:
        data = json.load(jsonFile)

    return jsonify(data["properties"])


@app.route('/property', methods=['POST'])
def property_find():
    # Input validation
    error = {}
    if (not request.json):
        error["input"] = "No input provided"
        return jsonify(error)
    if (not request.json["id"]):
        error["id"] = "Property id is required"
        return jsonify(error)

    # Open database
    with open("data.json", "r") as jsonFile:
        data = json.load(jsonFile)

    foundProperty = False
    for prop in data["properties"]:
        if (prop["id"] == request.json["id"]):
            foundProperty = prop

    if (not foundProperty):
        error['property'] = "Property not found"
        return jsonify(error)

    return jsonify(foundProperty)


@app.route('/property/create', methods=['POST'])
def property_create():
    # Input validation
    error = {}
    if (not request.json):
        error["input"] = "No input provided"
        return jsonify(error)
    if (not request.json["username"]):
        error["username"] = "Username is required"
    if (not request.json["password"]):
        error["password"] = "Password is required"
    if (not request.json["name"]):
        error["name"] = "Property name is required"
    if (not request.json["numberOfRooms"]):
        error["numberOfRooms"] = "Property number of rooms is required"
    if (not request.json["numberOfBedRooms"]):
        error["numberOfBedRooms"] = "Property number of bed rooms is required"
    if (error):
        return jsonify(error)

    # Get user from request
    loggedUser = {
        "username": request.json["username"],
        "password": request.json["password"]
    }

    foundUser = False
    # Check if username exists
    for user in loggedUsers:
        if (user["username"] == loggedUser["username"]):
            foundUser = user

    # Check if user not found
    if (foundUser == False):
        error["user"] = "User not logged"
        return jsonify(error)
    
    # Check if password is correct
    if (user["password"] != loggedUser["password"]):
        error["password"] = "Password is incorrect"
        return jsonify(error)

    # Get property from request
    newProperty = {
        "name": request.json["name"],
        "numberOfRooms": request.json["numberOfRooms"],
        "numberOfBedRooms": request.json["numberOfBedRooms"]
    }

    # Open database
    with open("data.json", "r") as jsonFile:
        data = json.load(jsonFile)
    
    # Add new id
    newProperty["id"] = str(int(data["lastPropertyId"]) + 1)
    # Refresh the property counter id
    data["lastPropertyId"] = newProperty["id"]
    # Add the new property to db
    data["properties"].append(newProperty)

    # Save to existing database
    with open("data.json", "w") as jsonFile:
        json.dump(data, jsonFile)

    return jsonify(newProperty)


@app.route('/property/update', methods=['POST'])
def property_update():
    # Input validation
    error = {}
    if (not request.json):
        error["input"] = "No input provided"
        return jsonify(error)
    if (not request.json["id"]):
        error["id"] = "Property id is required"
    if (not request.json["username"]):
        error["username"] = "Username is required"
    if (not request.json["password"]):
        error["password"] = "Password is required"
    if (not request.json["name"]):
        error["name"] = "Property name is required"
    if (not request.json["numberOfRooms"]):
        error["numberOfRooms"] = "Property number of rooms is required"
    if (not request.json["numberOfBedRooms"]):
        error["numberOfBedRooms"] = "Property number of bed rooms is required"
    if (error):
        return jsonify(error)

    # Get user from request
    loggedUser = {
        "username": request.json["username"],
        "password": request.json["password"]
    }

    foundUser = False
    # Check if username exists
    for user in loggedUsers:
        if (user["username"] == loggedUser["username"]):
            foundUser = user

    # Check if user not found
    if (foundUser == False):
        error["user"] = "User not logged"
        return jsonify(error)
    
    # Check if password is correct
    if (user["password"] != loggedUser["password"]):
        error["password"] = "Password is incorrect"
        return jsonify(error)

    # Open database
    with open("data.json", "r") as jsonFile:
        data = json.load(jsonFile)
    
    foundObject = False
    for i, prop in enumerate(data["properties"]):
        if (data["properties"][i]["id"] == request.json["id"]):
            data["properties"][i]["name"] = request.json["name"]
            data["properties"][i]["numberOfRooms"] = request.json["numberOfRooms"]
            data["properties"][i]["numberOfBedRooms"] = request.json["numberOfBedRooms"]
            foundObject = data["properties"][i]

    if (not foundObject):
        error["id"] = "Property id is incorrect"
        return jsonify(error)

    # Save to existing database
    with open("data.json", "w") as jsonFile:
        json.dump(data, jsonFile)

    return jsonify(foundObject)


@app.route('/property/delete', methods=['DELETE'])
def property_delete():
    # Input validation
    error = {}
    if (not request.json):
        error["input"] = "No input provided"
        return jsonify(error)
    if (not request.json["id"]):
        error["id"] = "Property id is required"
    if (not request.json["username"]):
        error["username"] = "Username is required"
    if (not request.json["password"]):
        error["password"] = "Password is required"
    if (error):
        return jsonify(error)

    # Get user from request
    loggedUser = {
        "username": request.json["username"],
        "password": request.json["password"]
    }

    foundUser = False
    # Check if username exists
    for user in loggedUsers:
        if (user["username"] == loggedUser["username"]):
            foundUser = user

    # Check if user not found
    if (foundUser == False):
        error["user"] = "User not logged"
        return jsonify(error)
    
    # Check if password is correct
    if (user["password"] != loggedUser["password"]):
        error["password"] = "Password is incorrect"
        return jsonify(error)

    # Open database
    with open("data.json", "r") as jsonFile:
        data = json.load(jsonFile)
    
    foundObject = False
    for i, prop in enumerate(data["properties"]):
        if (data["properties"][i]["id"] == request.json["id"]):
            foundObject = data["properties"][i]
            data["properties"].remove(data["properties"][i])

    if (not foundObject):
        error["id"] = "Property id is incorrect"
        return jsonify(error)

    # Save to existing database
    with open("data.json", "w") as jsonFile:
        json.dump(data, jsonFile)

    return jsonify({"message":"The property " + foundObject["name"] + " was removed"})



if __name__ == '__main__':
    app.run()
